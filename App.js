import React from 'react';
import moment from 'moment';
import {
    Text, View, FlatList, TouchableHighlight, Alert,
    TextInput, Picker, Button, TouchableOpacity
} from 'react-native';

import Menu from './src/components/Menu.js';
import Settings from './src/components/settings.js';
import styles from './src/styles/app.js';
import measurements from './src/units.js';
import { connect, connectID } from './src/connection/mLab_conn.js';

// Main component App.
class App extends React.Component {
    constructor() {
        super();
        this.state = {

        // page: 0 means the most recent list in the array.
            page: 0,

        // Holds temporary value for input fields.
            input: '',

        // Temporary value for Picker.
            picker: '',

        // Receives an array of JSON objects from mLab API.
            lists: undefined,

        // When set to true, this will enable creating a new list.
            create_new: false,

        // Includes items(only) to a new list. Id and timestamp are added later.
            new_list_items: [],

        // Sets the settings screen to visible/hidden.
            settings: false,

        // 3 variables hold values for mLab connection: database name, collection name and API key.
            db: '',
            col: '',
            key: ''
        }
    };

// Def function for fetching every list in database.
    fetchLists = async () => {
        const url = await connect();
        await console.log(url);
        await this.setState({lists: []});
        try {
            await fetch(url, {
                method: 'GET'
            })
                .then(res => res.json())
                .then(res => {
                    this.setState({lists: res.reverse()})
                })
                .catch(err => {
                    throw err
                })
        } catch (e) {
            console.log(e);
        }
    };

// Def function for setting AsyncStorage items to App's state
saveAsync = (name, value) => {
    switch (name) {
        case 'db': this.setState({db: value}); break;
        case 'col': this.setState({col: value}); break;
        case 'key': this.setState({key: value}); break;
    }
};

// Def function that enables creating a new list.
    createNewList = () => {
        this.setState({create_new: this.state.create_new === false})
    };

// Def function for deleting the current list by id.
    deleteList = () => {
        Alert.alert(
            'Deleting list "'+this.listCounter()+'"',
            'Are you sure you want to remove this list permanently?',
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'OK', style: 'ok',
                    onPress: async () => {
                    if (this.state.lists[this.state.page] !== undefined) {
                        const url = await connectID(this.state.lists[this.state.page]["_id"]["$oid"]);
                        await fetch( url, {method: 'DELETE'})
                            .then(this.fetchLists)
                            .catch(err => {throw err});
                        this.setState({page: 0})
                    } else {
                        Alert.alert(
                            '404',
                            'No content found. There were no defined items stored.'
                        )
                    }}
                }
            ]
        )
    };

// Def function that returns the current list's index as a string.
    listCounter = () => {
        if (this.state.page === 0) {
            return "Recent"
        }
        return this.state.page + 1 + "/" + this.state.lists.length
    };

// Sets timeout to 0.8s and then fetches lists from mLab.
    componentWillMount() {
        this.fetchLists()
    };

// Just to make sure page doesn't render while typing to input field.
    shouldComponentUpdate(nextProp, nextState) {
        return this.state.input === nextState.input
    }

// Def function that pops up settings.
    openSettings = () => {
        this.setState({settings: this.state.settings === false})
    };

    render() {

    // Def component ListMapper for creating a scrollable list out of all the given items.
        const ListMapper = () => {
            let new_state = this.state.lists;
            if (this.state.lists !== undefined && this.state.lists.length > 0) {

            // Def function for updating parts of an existing list.
                const updateList = async (i, part) => {

                // The same function works for both input fields.
                    if (part === 'subject') {
                        new_state[this.state.page].items[i.key].subject.editmode = false;
                        new_state[this.state.page].items[i.key].subject.name = this.state.input;
                    }
                    if (part === 'count') {
                        new_state[this.state.page].items[i.key].count.editmode = false;
                        new_state[this.state.page].items[i.key].count.amount = this.state.input;
                        new_state[this.state.page].items[i.key].count.unit = this.state.picker;
                    }
                    this.setState({lists: new_state});

                // Updating database via PUT request.
                    const url = await connectID(this.state.lists[this.state.page]["_id"]["$oid"]);
                    await fetch( url, {
                            method: 'PUT',
                            headers: {'Content-Type': 'application/json'},
                            body: JSON.stringify(this.state.lists[this.state.page])
                        }
                    ).catch(err => {throw err})
                };

            // Displays the item key, which is also used for index.
                const ItemKey = ({i}) => {
                    return (
                        <Text style={styles.itemKey}>
                            {i.key}.
                        </Text>
                    )
                };

            // Def component to display and modify item's subject.
                const ItemSubject = ({i}) => {
                    if (!i.subject.editmode) {
                        return (
                            <TouchableHighlight
                                style={styles.itemSubject}
                                underlayColor={'#ebebeb'}
                                onLongPress={() => {
                                    const new_state = this.state.lists;
                                    new_state[this.state.page].items[i.key].subject.editmode = true;
                                    this.setState({lists: new_state});
                                }}>
                                <Text style={styles.itemSubjectText}>
                                    {i.subject.name}
                                </Text>
                            </TouchableHighlight>
                        )
                    } else {
                        return <View style={styles.itemSubjectEdit}>
                            <TextInput style={styles.itemSubjectInput}
                               multiline={true}
                               placeholder={i.subject.name}
                               onChangeText={input => {
                                   this.setState({input})
                               }}
                               onSubmitEditing={() => {updateList(i, 'subject')}}
                               onEndEditing={() => {updateList(i, 'subject')}}/>
                        </View>
                    }
                };

            // Def component for editing item's amount and units.
                const ItemUnits = ({i}) => {
                    if (!i.count.editmode) {
                        return (
                            <TouchableHighlight
                                style={styles.itemUnits}
                                underlayColor={'#ebebeb'}
                                onLongPress={() => {
                                    const new_state = this.state.lists;
                                    new_state[this.state.page].items[i.key].count.editmode = true;
                                    this.setState({lists: new_state});
                                }}>
                                <Text style={styles.itemUnitsText}>
                                    {i.count.amount}{' ' + i.count.unit}
                                </Text>
                            </TouchableHighlight>
                        )
                    } else {
                        return (
                            <View style={styles.itemUnitsEdit}>
                                <TextInput
                                    style={styles.unitsEditInput}
                                    placeholder={i.count.amount}
                                    onChangeText={input => {
                                        this.setState({input})
                                    }}
                                    onFocus={() => {
                                // Setting input back to empty string.
                                        if (this.state.input !== '') {
                                            this.setState({input: ''})
                                        }
                                    }}
                                    onSubmitEditing={() => {updateList(i, 'count')}}
                                    onEndEditing={() => {updateList(i, 'count')}}/>
                                <Text style={styles.unitsEditText}>
                                    {this.state.picker}
                                </Text>
                                <Picker
                                    selectedValue={this.state.picker}
                                    onValueChange={value => {
                                        this.setState({picker: value})
                                    }}>
                                    {measurements.map(unit => <Picker.Item key={unit} label={unit} value={unit}/>)}
                                </Picker>
                            </View>
                        )
                    }
                };

            // Returns structure: View | FlatList | View
                return (
                    <View style={{flex: 1, margin: 8, paddingBottom: 82}}>

                        <View style={{
                    /* The first View shows the list's timestamp */ flexDirection: "column"}}>
                            <View style={styles.timeStamp}>
                                <Text style={{flex: 1, textAlign: "center"}}>✿</Text>
                                <Text style={{flex: 7, fontSize: 15, color: "black", textAlign: "center"}}>
                                    {this.state.lists[this.state.page].date}
                                </Text>
                                <Text style={{flex: 1, textAlign: "center"}}>✿</Text>
                            </View>

                            <FlatList data={
                        /* FlatList maps every item in list */
                                this.state.lists[this.state.page].items
                            }
                                      renderItem={({item}) => {
                                          return (
                                              <View key={item.key} style={{flex: 1, flexDirection: "row"}}>

                                                  <ItemKey i={item}/>

                                                  <ItemSubject i={item}/>

                                                  <ItemUnits i={item}/>

                                              </View>
                                          )
                                      }}/>

                            <View style={ styles.listNavigation
                        /* The second View includes listCounter and arrows for navigation */}>
                                <View style={{ flex: 1, justifyContent: "center" }}>
                                    <Text style={{ textAlign: "center", textAlignVertical: "center", fontSize: 18}}
                                          onPress={() => {
                                          if (this.state.page < this.state.lists.length - 1) {
                                              this.setState({page: this.state.page + 1})
                                          }
                                      }}>
                                        ◀
                                    </Text>
                                </View>
                                <Text
                                    style={styles.listCounter}>
                                    {this.listCounter()}
                                </Text>
                                <View style={{ flex: 1, justifyContent: "center" }}>
                                    <Text style={{ textAlign: "center", textAlignVertical: "center", fontSize: 18 }}
                                          onPress={() => {
                                              if (this.state.page > 0) {
                                                  this.setState({page: this.state.page - 1})
                                              }
                                      }}>
                                        ▶
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                )
            } else {

            // This Text is usually visible for one or two seconds before promise is fulfilled,
            // but will stay if the collection was empty or the connection to database was unsuccessful.
                return <Text style={{textAlign: "center", textAlignVertical: "center", margin: 12}}>
                    Waiting for response{"\n"}This may take up to 3-5 seconds
                </Text>
            }
        };

    // Def component to switch between ListMapper and AddList.
        const ListContainer = () => {
            if (this.state.create_new) {
                return <AddList/>
            } else {
                return <ListMapper/>
            }
        };

    // Def component for creating and saving new lists.
        const AddList = () => {

        // Def component for adding items to the new list.
            const AddItem = () => {
                return <TouchableOpacity
                    style={{alignItems: 'center'}}
                    activeOpacity={0.7}
                    onPress={() => {
                        const new_state = this.state.new_list_items;
                        new_state.push(
                            {
                                key: this.state.new_list_items.length.toString(),
                                subject: {
                                    name: '',
                                    editmode: false
                                },
                                count: {
                                    amount: '',
                                    unit: '',
                                    editmode: false
                                }
                            }
                        );
                        this.setState({new_list_items: new_state})
                    }}>
                    <Text style={styles.addItem}>
                        +
                    </Text>
                </TouchableOpacity>
            };

        // Def component Submit-button. Posts the new list to mLab API
            const Submit = () => {
              return <View style={{padding: 5}}>
                  <Button
                      title={"Submit"}

                      onPress={ async () => {
                          const url = await connect();
                          await fetch( url, {
                                  method: 'POST',
                                  headers: {'Content-Type': 'application/json'},
                                  body: JSON.stringify(
                                      {
                                          date: moment().format("HH:mm:ss DD-MM-YYYY"),
                                          items: this.state.new_list_items
                                      })
                              })
                              .then(res => {
                                  if (res.status == 200) {
                                      this.setState({new_list_items: []});

                                  // Sends Alert message, brings user back to main view and fetches updated lists from database.
                                      this.createNewList();
                                      this.fetchLists();
                                      Alert.alert(
                                          'New list created',
                                          'A new list successfully sent to the server.'
                                      )
                                  } else {
                                      Alert.alert(
                                          'Error',
                                          'Something went wrong while creating a list.'
                                      )
                                  }
                              }).catch(err => {throw err})
                      }}/>
              </View>
            };

        // Checking if there are any items in new_list_items.
            if (this.state.new_list_items !== undefined && this.state.new_list_items.length > 0) {
                return (
                <View style={{paddingBottom: 74}}>
                    <View style={styles.newListForm}>

                        <Text style={ styles.newListTimeStamp
                    /* Current date and time. Placed on top of the form. */}>
                            Time and Date: {moment().format("HH:mm:ss DD-MM-YYYY")}
                        </Text>

                        <View style={{flexDirection: 'row'}
                    /* Description for FlatList rows: Name | Amount | Units */}>
                            <Text style={[{flex: 2}, styles.description]}>
                                Item name
                            </Text>
                            <Text style={[{flex: 1}, styles.description]}>
                                Amount
                            </Text>
                            <Text style={[{flex: 1}, styles.description]}>
                                Units
                            </Text>
                        </View>

                        <FlatList data={this.state.new_list_items}
                            renderItem={({item, index}) => {

                                let new_state = this.state.new_list_items;
                                new_state[index].key = index.toString();

                                return (
                                    <View key={({index})}
                                        style={{flexDirection: "row"}}>
                                        <TextInput style={styles.itemNameInput}
                                            multiline={true}
                                            placeholder={item.subject.name}
                                            placeholderTextColor={'#373737'}
                                            onChangeText={input => {
                                                this.setState({input})
                                            }}
                                            onSubmitEditing={() => {
                                                new_state[index].subject.name = this.state.input;
                                                this.setState({new_list_items: new_state})
                                            }}
                                            onEndEditing={() => {
                                                new_state[index].subject.name = this.state.input;
                                                this.setState({new_list_items: new_state})
                                            }}/>
                                        <TextInput
                                            style={styles.itemAmountInput}
                                            multiline={true}
                                            placeholder={item.count.amount}
                                            placeholderTextColor={'#373737'}
                                            onChangeText={input => {
                                                this.setState({input})
                                            }}
                                            onSubmitEditing={() => {
                                                new_state[index].count.amount = this.state.input;
                                                this.setState({new_list_items: new_state})
                                            }}
                                            onEndEditing={() => {
                                                new_state[index].count.amount = this.state.input;
                                                this.setState({new_list_items: new_state})
                                            }}/>

                                        <View style={{ flex: 1, flexDirection: "row"}}>

                                            <View style={{flex: 1}}>
                                                <Picker
                                                    onValueChange={value => {
                                                        new_state[index].count.unit = value;
                                                        this.setState({new_list_items: new_state})
                                                    }}>
                                                    {measurements.map(unit => <Picker.Item key={unit} label={unit} value={unit}/>)}
                                                </Picker>
                                            </View>

                                            <Text style={styles.unitsLabel}>
                                                {this.state.new_list_items[index].count.unit}
                                            </Text>

                                            <View style={styles.itemDelete
                                        /* Button for deleting the item from list */}>
                                                <Text style={styles.itemDeleteLabel}
                                                    onPress={() => {
                                                        new_state.splice(index, 1);
                                                        this.setState({new_list_items: new_state})
                                                    }}>
                                                    x
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                )
                            }}/>

                        <AddItem/>

                        <Submit/>

                    </View>
                </View>
                )
            } else {
                return (
                    <View style={{alignContent: 'center', margin: 8}}>
                        <AddItem/>
                    </View>
                )
            }
        };
        return (
            <View style={{flex: 1, flexDirection: "column"}}>
                <View style={{height: 74, backgroundColor: "#ffe22a", flexDirection: "row"}}>
                    <Text style={styles.headerText}>
                       Shopping List
                    </Text>
                </View>
                <ListContainer/>
                <Menu
                    del_list={this.deleteList}
                    new_list={this.createNewList}
                    settings={this.openSettings}
                    refresh={this.fetchLists}
                />
                <Settings
                    settings={this.state.settings}
                    openSettings={this.openSettings}
                    saveAsync={this.saveAsync}
                    async={[this.state.db, this.state.col, this.state.key]}
                />
            </View>
        );
    }
}

export default App;
