import { StyleSheet } from 'react-native';

const styles = StyleSheet.create(
    {
        rootView: {

            position: 'absolute',
            top: 0,
            right: 0,
            height: 280,
            width: 100
        },
        buttonOrange: {

            position: 'absolute',
            marginRight: 6,
            right: 0
        },
        buttonDelete: {

            position: 'absolute',
            backgroundColor: '#ffe22a',
            borderRadius: 30,
            top: 80,
            padding: 12,
        },
        buttonDelText: {

            color: '#ffffff',
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 14
        },
        addNewList: {

            position: 'absolute',
            backgroundColor: '#ffe22a',
            borderRadius: 30,
            top: 130,
            padding: 12
        },
        addNewListText: {

            color: '#ffffff',
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 14
        },
        settings: {

            position: 'absolute',
            backgroundColor: '#ffe22a',
            borderRadius: 30,
            top: 180,
            padding: 12
        },
        settingsText: {

            color: '#ffffff',
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 14
        },
        refresh: {

            position: 'absolute',
            backgroundColor: '#ffe22a',
            borderRadius: 30,
            top: 230,
            padding: 12
        },
        refreshText: {

            color: '#ffffff',
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 14
        }
    }
);

export default styles;
