import { StyleSheet } from 'react-native';

const styles = StyleSheet.create(
    {
    // Settings View.
        settings: {

            overflow: 'scroll',
            position: "absolute",
            backgroundColor: 'rgba(0,0,0,0.7)',
            borderRadius: 30,
            paddingRight: 30,
            paddingLeft: 30,
            margin: 16
        },
        settingsInput: {

            backgroundColor: 'rgba(255,255,255,0.5)',
            minWidth: 120,
            borderWidth: 1,
            borderRadius: 12,
            fontSize: 16,
            padding: 2,
            margin: 4
        },

    // ItemKey component.
        itemKey: {

            flex: 2,
            fontSize: 18,
            borderWidth: 1,
            textAlign: "center",
            justifyContent: "center",
            textAlignVertical: "center",
            padding: 3,
            margin: 4,
        },

    // ItemSubject component.
        itemSubject: {

            flex: 8,
            borderWidth: 1,
            justifyContent: "center",
            margin: 4
        },
        itemSubjectText: {

            fontSize: 18,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 4
        },
        itemSubjectEdit: {

            flex: 8,
            borderWidth: 1,
            margin: 4
        },
        itemSubjectInput: {

            fontSize: 18,
            padding: 10
        },

    // ItemUnits component.
        itemUnits: {

            flex: 3,
            borderWidth: 1,
            justifyContent: "center",
            margin: 4
        },
        itemUnitsText: {

            fontSize: 18,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 4,
            paddingRight: 8,
            paddingLeft: 8
        },
        itemUnitsEdit: {

            flex: 3,
            borderWidth: 1,
            justifyContent: "center",
            margin: 4
        },
        unitsEditInput: {

            fontSize: 18,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 4,
            margin: 4
        },
        unitsEditText: {

            fontSize: 18,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 2,
            margin: 2
        },

// The main list components.
        timeStamp: {

            flexDirection: "row",
            borderTopRightRadius: 8,
            borderTopLeftRadius: 8,
            borderWidth: 1,
            padding: 6,
            margin: 4
        },
        listNavigation: {

            borderBottomRightRadius: 8,
            borderBottomLeftRadius: 8,
            flexDirection: "row",
            borderWidth: 1,
            padding: 4,
            margin: 4
        },
        listCounter: {

            flex: 6,
            textAlign: "center",
            textAlignVertical: "center",
            fontSize: 16
        },

    // AddItem button-component.
        addItem: {

            color: '#e4e0ff',
            backgroundColor: '#47a512',
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 18,
            borderRadius: 16,
            height: 40,
            width: 80,
            padding: 8,
            margin: 8
        },

    // The new list creation form.
        newListForm: {

            overflow: 'scroll',
            borderWidth: 1,
            borderRadius: 8,
            flexDirection: "column",
            margin: 10
        },
        newListTimeStamp: {

            fontSize: 14,
            borderBottomWidth: 1,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 8
        },

    // Descriptions for FlatList rows.
        description: {

            fontSize: 16,
            borderBottomWidth: 1,
            textAlign: "center",
            textAlignVertical: "center",
            fontWeight: "bold",
            padding: 7
        },

    // Inputs for FlatList.
        itemNameInput: {

            flex: 2,
            fontSize: 16,
            padding: 8,
            margin: 4
        },
        itemAmountInput: {

            flex: 1,
            fontSize: 16,
            padding: 8,
            margin: 4
        },

    // Label for showing the Picker's selection.
        unitsLabel: {

            flex: 1,
            fontSize: 16,
            textAlign: "center",
            textAlignVertical: "center",
            padding: 2
        },

    // Small delete button for each item in list.
        itemDelete: {

            position: 'absolute',
            backgroundColor: '#d20003',
            alignContent: 'center',
            justifyContent: 'center',
            top: 1,
            right: 1,
            width: 20,
            height: 20,
            borderRadius: 10
        },
        itemDeleteLabel: {

            fontSize: 14,
            color: '#fff',
            textAlign: "center",
            padding: 4
        },


        headerText: {
            flex: 1,
            color: '#fafafa',
            textAlignVertical: "center",
            fontWeight: 'bold',
            fontSize: 28,
            paddingLeft: 12,
            margin: 8
        }
    }
);

export default styles;
