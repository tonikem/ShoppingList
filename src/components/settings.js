import React from 'react';
import {
    View, Text, TextInput, StyleSheet,
    AsyncStorage, Button, ScrollView
} from "react-native";

import styles from "../styles/app";

// Def component for settings.
const Settings = ({settings, openSettings, saveAsync, async}) => {
    if (settings) {
        return (
            <ScrollView style={[styles.settings, StyleSheet.absolutefillobject]}>

                <Text style={{fontSize: 30, color: 'white', fontWeight: 'bold', margin: 16}}>
                    Settings
                </Text>

                <Text style={{fontSize: 18, color: 'white'}}>
                    Database name
                </Text>
                <TextInput
                    placeHolder={async[0]}
                    style={styles.settingsInput}
                    onChangeText={value => {
                        saveAsync('db', value)
                    }}/>

                <Text style={{fontSize: 18, color: 'white'}}>
                    Collection name
                </Text>
                <TextInput
                    placeHolder={async[1]}
                    style={styles.settingsInput}
                    onChangeText={value => {
                        saveAsync('col', value)
                    }}/>

                <Text style={{fontSize: 18, color: 'white'}}>
                    API key
                </Text>
                <TextInput
                    placeHolder={async[2]}
                    style={[styles.settingsInput, {marginBottom: 16}]}
                    onChangeText={value => {
                        saveAsync('key', value)
                    }}/>

                <Button
                    title={'Save'}
                    onPress={ () => {
                    // Saves db, col and key from App's state to AsyncStorage.
                        AsyncStorage.setItem("db", async[0]);
                        AsyncStorage.setItem("col", async[1]);
                        AsyncStorage.setItem("key", async[2]);
                        openSettings();
                    }}/>

                <Text
                    onPress={openSettings}
                    style={{
                        color: 'white',
                        fontSize: 14, margin: 18
                    }}>
                    Cancel
                </Text>
            </ScrollView>
        )
    } else {
    // Returns empty View when set to false.
        return <View/>
    }
};

export default Settings;
