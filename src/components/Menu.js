import React from 'react';
import { View, Animated, TouchableOpacity } from 'react-native';

import styles from '../styles/menu.js';


// Menu component. Includes buttons for opening menu, for settings,
// for adding new lists and for deleting and refreshing lists.
class Menu extends React.Component {
    constructor(props) {
        super(props);
    }
    state = {

    // Menu icon is facing up when the value is 0. The value after full rotation(160deg) is 1.
        menu_value: new Animated.Value(0),

    // The state indicates whether the menu is supposed to be open or closed.
        menu_state: false   // false = menu is not opened.

    };

// Def function for handling onPress event for menu.
    menuPushed = () => {

        if (!this.state.menu_state) {
            this.setState({menu_state: true});
            Animated.timing(
                this.state.menu_value,
                {toValue: 1, duration: 400, useNativeDriver: true}
            ).start();
        } else {
            this.setState({menu_state: false});
            Animated.timing(
                this.state.menu_value,
                {toValue: 0, duration: 400, useNativeDriver: true}
            ).start();
        }
    };
    render() {

    // Bringing props: deleteList, createNewList and fetchList from App component.
        const { del_list, new_list, settings, refresh } = this.props;

    // Interpolating rotation for menu icon.
        const rotation = this.state.menu_value.interpolate({
            inputRange: [0, 1],
            outputRange: ['50deg', '-110deg']
        });

    // 4 menu options that slide in from the right at the same time.
        const slide_deleteThis = this.state.menu_value.interpolate({
            inputRange: [0, 1],
            outputRange: [100, 0]
        });
        const slide_addNew = this.state.menu_value.interpolate({
            inputRange: [0, 1],
            outputRange: [160, 10]
        });
        const slide_settings = this.state.menu_value.interpolate({
            inputRange: [0, 1],
            outputRange: [220, 14]
        });
        const slide_refresh = this.state.menu_value.interpolate({
            inputRange: [0, 1],
            outputRange: [280, 18]
        });

        return (
            <View style={styles.rootView}>
                <TouchableOpacity style={styles.buttonOrange} activeOpacity={
            /* Orange-shaped menu icon */
                    0.6 } onPress={this.menuPushed}>
                    <Animated.Image
                        style={{width: 64, height: 64, margin: 4, transform: [{rotate: rotation}]}}
                        source={{uri: "https://cdn2.iconfinder.com/data/icons/shiny-fruits/100/orange-3-128.png"}}
                    /></TouchableOpacity>

                <TouchableOpacity style={[styles.buttonDelete, {transform: [{translateX: slide_deleteThis}]}]} activeOpacity={
            /* Button for deleting current list. */
                    0.8 } onPress={del_list /* <- this.props.del_list */}>
                    <Animated.Text style={styles.buttonDelText}>
                        Delete this
                    </Animated.Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.addNewList, {transform: [{translateX: slide_addNew}]}]} activeOpacity={
            /* Button for adding new list. */
                    0.8 } onPress={() => {
                        new_list(); /* <- this.props.new_list */
                        this.menuPushed()
                    }}>
                    <Animated.Text style={styles.addNewListText}>
                        Add New
                    </Animated.Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.settings, {transform: [{translateX: slide_settings}]}]} activeOpacity={
            /* Button for opening settings. */
                    0.8 } onPress={() => {
                        settings(); /* <- this.props.settings */
                        this.menuPushed()
                    }}>
                    <Animated.Text style={styles.settingsText}>
                        Settings
                    </Animated.Text>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.refresh, {transform: [{translateX: slide_refresh}]}]} activeOpacity={
            /* Button for refreshing the page. */
                    0.8} onPress={refresh /* <- this.props.refresh */}>
                    <Animated.Text style={styles.refreshText}>
                        Refresh
                    </Animated.Text>
                </TouchableOpacity>

            </View>
        )
    }
}

export default Menu;
