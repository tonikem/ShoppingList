// Storing units of measurement in an array for Picker.
const measurement = ['', 'l', 'cl', 'dl', 'ml', 'kg', 'g', 't', 'tbs.', 'c', 'gal', 'lb', 'oz'];

export default measurement;
