import { AsyncStorage } from 'react-native';


// Def function for connecting to API with database name and collection name.
const connect = async () => {
    const db_name = await AsyncStorage.getItem("db");
    const col_name = await AsyncStorage.getItem("col");
    const api_key = await AsyncStorage.getItem("key");
    return `https://api.mlab.com/api/1/databases/${db_name}/collections/${col_name}?apiKey=${api_key}`
};

// Def function for fetching a specific list from database using id.
const connectID = async (id) => {
    const db_name = await AsyncStorage.getItem("db");
    const col_name = await AsyncStorage.getItem("col");
    const api_key = await AsyncStorage.getItem("key");
    return `https://api.mlab.com/api/1/databases/${db_name}/collections/${col_name}/${id}?apiKey=${api_key}`
};

module.exports = { connect, connectID };
