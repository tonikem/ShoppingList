<h1>React Native Ostoslista</h1>

Tämä ostoslista on toteutettu puhtaasti React Native:lla. Tietokantana toimii MongoDB:n pohjautuva https://mlab.com.

Alla näytönkaappauksia ohjelman käytöstä:

<img src="https://i.imgur.com/bKjfR8v.png">
<img src="https://i.imgur.com/swzwNB9.png">
<img src="https://i.imgur.com/6Y8cK0A.png">
